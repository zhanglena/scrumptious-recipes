from django.contrib import admin
from receipes.models import Recipe, Step, Measure, Ingredient, FoodItem

admin.site.register(Recipe)
admin.site.register(Step)
admin.site.register(Measure)
admin.site.register(Ingredient)
admin.site.register(FoodItem)
# Register your models here.
